import os, sys, requests, yaml

from tradingview_ta import TA_Handler
from tradingview_ta import Interval

try:
    with open("bot.yaml", "r") as bot_cfg:
        config = yaml.load(bot_cfg, Loader=yaml.FullLoader)
        print("tvbot: config read successful")
except:
    print("tvbot: config read failed", file=sys.stderr)
    sys.exit(1)

try:
    slack_webhook = os.environ['TVBOT_SLACK_WEBHOOK']
except:
    print("tvbot: error reading TVBOT_SLACK_WEBHOOK envvar", file=sys.stderr)
    sys.exit(1)

color_mapping_signals = {
    "sell": "#fc0905",
    "buy": "#36a64f",
    "neutral": "#fceb05"
}


def get_analysis(symbol, screener, exchange):
    print("running analysis on %s" % symbol)
    interval = Interval.INTERVAL_1_DAY

    handler = TA_Handler(
        symbol=symbol,
        screener=screener,
        exchange=exchange,
        interval=interval
    )

    analysis = handler.get_analysis()
    print("analysis summary: %s" % str(analysis.summary))
    return analysis

def send_slack_message(slack_webhook, message_payload):

    slack_message = {
        "text": "Exchange *%s*" % config["tradingview"]["exchange"],
        "username": "TVbot",
        "channel": config["slack"]["channel"],
        "icon_emoji": "shut-up-and-take-my-money",
        "attachments": message_payload
    }

    r = requests.post(slack_webhook, json=slack_message)
    print(slack_message)
    print(r.status_code)
    return r.status_code

def retrieve_data(symbol_list=config['tradingview']['symbols']):

    analysis_list=[]

    for symbol in symbol_list:
        try:
            analysis_list.append(get_analysis(symbol=symbol, 
                screener=config['tradingview']['screener'],
                exchange=config['tradingview']['exchange']
                ))
        except:
            print("tvbot: failed to get analysis for %s" % symbol, file=sys.stderr)
    
    return analysis_list

def create_payload(analysis_list):
    message_payload = []
    for analysis_item in analysis_list:
        message_payload.append({
            "title": analysis_item.symbol,
            "title_link": "https://www.tradingview.com/symbols/%s/technicals/?exchange=%s" % (analysis_item.symbol, analysis_item.exchange),
            "text": "Recommendation: %s (Buy: %i, Sell: %i, Neutral: %i)" % 
                (
                    analysis_item.summary['RECOMMENDATION'], 
                    analysis_item.summary['BUY'],
                    analysis_item.summary['SELL'],
                    analysis_item.summary['NEUTRAL']
                    ),
            "color": color_mapping_signals[(analysis_item.summary['RECOMMENDATION']).lower()]
        })
    return message_payload

def main():
    print(config)
    analysis_data = retrieve_data()
    payload = create_payload(analysis_data)
    send_slack_message(slack_webhook, message_payload=payload)


if __name__ == "__main__": 
    # Executed when invoked directly
    main()
else:
    # Executed when imported
    pass






